\chapter{Introduction}
\section{Overview}
Elementary number theory deals with some properties of the elements of the ring of integers $\mathbb{Z}$ and the field of the rational numbers, $\mathbb{Q}$. It is interesting to note that the ring $\mathbb{Z}$ and the polynomial ring $\mathbb{F}_q[T]$ have many remarkable similarities. As we shall see soon, such similarities exist in the following areas;
\begin{itemize}
\item Principal ideal domain,
\item Units,
\item Number of prime elements in these rings,
\item Arithmetic problems and much more.
\end{itemize}
Consequently, this essay is dedicated to explore such similarities. Before we start to do so, we provide the basic information of the essay.
\section{Preliminaries}
\label{pre:l}
In the following, $\mathbb{F}_q$ denotes the finite field of order $q$ in one variable. Generally, the number of elements for any finite field is $q=p^m$ for some prime number $p$ and some integer $m\geq 1$. We call $p$ the characteristic of the field and $m$ the dimension of the field. For the definition of the field and its properties, see \citet{roman2006field} p.22. 

In this essay, by "ring", we actually mean a commutative ring with identity and in what follows, we shall use $\mathbb{A}$ to mean the ring $\mathbb{Z}$ or $\mathbb{F}_q[T]$ and "prime" to mean a prime number when $\mathbb{A}=\mathbb{Z}$ and "a monic irreducible polynomial", if $\mathbb{A} = \mathbb{F}_q[T]$.
%\begin{defn}
%A set $\mathbf{R}$ together with binary operation $+$ and $*$ is called a commutative ring with identity if the following addition and multiplication properties are met;
%\begin{itemize}
%\item[(i)] addition and multiplication are are \textit{associative}, i.e. 
%$\forall \alpha,\beta,\gamma \in \mathbf{R}, (\alpha + \beta) + \gamma = \alpha + (\beta + \gamma)$ and
% $(\alpha \cdot\beta)\cdot\gamma = \alpha\cdot(\beta\cdot\gamma) $. 
% \item[(ii)]addition and multiplication are \textit{commutative}, i.e. $\forall \alpha,\beta\in\mathbf{R}: \alpha + \beta = \beta + \alpha$ and $\alpha\cdot\beta = \beta\cdot\alpha$.
% \item[(iii)]additive \textit{inverses}, i.e $\exists -\alpha \in\mathbf{R}: \alpha + (-\alpha) = -\alpha + \alpha = 0$.
% \item[(iv)]addition and multiplicative \textit{identities},i.e. $\exists 0\in\mathbf{R},\forall \alpha\in\mathbf{R}: 0 + \alpha = \alpha + 0 = \alpha, \forall \alpha\in\mathbf{R}$ and $\exists 1\in\mathbf{R},\forall \alpha\in\mathbf{R}, 1\cdot\alpha = \alpha\cdot 1 = \alpha $.
%\end{itemize}
%\end{defn}

\begin{defn}
A ring $\mathbf{R}$ with only one maximal ideal is called \textit{a local ring.}
\end{defn}

\begin{defn}
An element $\mathcal{U}\in \mathbf{R}$ is said to be a \textit{unit} if there exists an element $\mathcal{V}\in\mathbf{R}$ such that $\mathcal{U}\mathcal{V}=1.$
\end{defn}
\begin{pro}
A ring $\mathbf{R}$ is said to be local if and only if non-units form an ideal.
\end{pro}
\begin{proof}
Let $\mathbf{R}$ be a local ring with maximal ideal $\mathit{M}$. Suppose that $\alpha$ is any element such that $\mathit{M}\notin \alpha$. This means that $\alpha$ should be a unit or else, $\alpha$ generates an ideal $\langle\alpha\rangle$ that is contained in a maximal ideal apart from $\mathit{M}$.

On the other hand, let $\mathbf{R}$ be a ring in which non-units form an ideal $I$. Then all ideals in $\mathbf{R}$ should be contained in $I$ because ideals cannot contain units. 
\end{proof}
%For the definition of the ring, refer to \citet{Rowland}.
\begin{defn}
A commutative ring $\mathbf{R}$ is said to be an \textit{integral domain} if for all non-zero $\alpha,\beta\in\mathbf{R}$, $\alpha\beta \in\mathbf{R}$ is also non-zero.
\end{defn}
For the definition of $\mathbb{A}/g\mathbb{A}$ refer to \citep{stein2005elementary}, p 22.
\begin{lem}
$\mathbb{A}/g\mathbb{A}$ is a commutative ring.
\end{lem}
\begin{proof}
We will show that $\mathbb{A}/g\mathbb{A}$ satisfies the properties of a commutative ring;
\begin{itemize}
\item[(1)] Commutative rule for addition and multiplication:
\begin{align*}
\forall \alpha, \beta \in\mathbb{A}/g\mathbb{A}; \alpha+\beta\equiv \beta + \alpha \pmod g \text{ and } \alpha\beta \equiv \beta\alpha \pmod g.
\end{align*}
\item[(2)] Associative rule for addition and multiplication:
\begin{align*}
\forall\alpha,\beta,\gamma\in\mathbb{A}/g\mathbb{A}; (\alpha+\beta)+\gamma\equiv\alpha+(\beta+\gamma)\equiv\pmod g \text{ and } (\alpha\beta)\gamma \equiv \alpha(\beta\gamma)\pmod g.
\end{align*}
\item[(3)]Additive identity: $\forall\alpha\in\mathbb{A}/g\mathbb{A}; \alpha \equiv \alpha + 0 \pmod g.$
\item[(4)]Additive inverse: 
\begin{itemize}
%\item[(i)]Let $\alpha\in\mathbb{A}g/\mathbb{A}$ where $\alpha$ is any congruence class modulo $g$.
%\item[(ii)]Let $\beta = g-\alpha.$
\item[(i)]$\forall\alpha\in\mathbb{A}/g\mathbb{A},\exists\beta\in\mathbb{A}/g\mathbb{A}$ with $1\leq\beta\leq g$ such that $\alpha + \beta\equiv g \equiv 0 \pmod g.$
\end{itemize}
\item[(5)]Distributive rule: $\forall\alpha,\beta,\gamma\in\mathbb{A}/g\mathbb{A}: \alpha(\beta+\gamma)\equiv\alpha\beta + \alpha\gamma \equiv (\beta + \gamma)\alpha \pmod g.$
\end{itemize}
Therefore, $\mathbb{A}/g\mathbb{A}$ is a commutative ring.
\end{proof}

The elements of a polynomial ring $\mathbb{F}_q[T]$ are of the form;
\begin{align*}
f(T) = \alpha_0T^{n} + \alpha_1T^{n-1} + \ldots + \alpha_n, \text{ for } n\in\mathbb{N}
\end{align*}
with $\alpha_i\in\mathbb{F}_q,$ for all $0\leq i\leq n.$ The $\alpha_i$ are the \textit{coefficients} of the polynomial $f(T)$ if $\alpha_0 \neq 0$. We denote the coefficient $\alpha_0$ of the polynomial $f(T)$ as \textit{sgn$(f)$} and we call $n$ the \textit{degree} of the polynomial $f(T)$ denoted as deg($f$).

On the other hand, $f(T)$ is said to be a \textit{non-zero} polynomial in $\mathbb{F}_q[T]$ if there exists an $\alpha_i \neq 0,$ for some $1\leq i\leq n$. 
In what follow, we shall use $f$ to mean $f(T)$.

\textbf{ Some properties of the elements in $\mathbb{F}_q[T]$}
 
Let $f_1$ and $f_2$ in $\mathbb{F}_q[T]$ be non-zero polynomials. Then,
\begin{enumerate}
\item[]
\begin{enumerate}[(i)]
	\item deg$(f_1f_2)$ = deg$(f_1)$ + deg$(f_2)$,
	\item sgn$(f_1f_2)$ = sgn($f_1$)sgn$(f_2)$,
	\item deg$(f_1 + f_2)$ $\leq$ max\Big(deg$(f_1)$,deg$(f_2)$\Big),
	\item if $f_1 = 0$, then sgn($f_1) = 0$,
	\item if $f_1 = 0$, then deg($f_1) = \infty$.
\end{enumerate}
\end{enumerate}	
	A polynomial $f$ is said to be monic if $sgn(f) = 1$.  For example, the polynomial $f = T^6 - 2T^3 + T + 4$ is a monic polynomial because the  coefficient of $T^6$ is 1.
Such polynomials are important in  this essay.














