\chapter{Riemann-Zeta Function and Arithmetic Functions}\label{ch:3}
\section{Zeta Function}
The aim of this chapter is to show the analogue of the classical prime \textit{number theorem} for polynomials. We shall also consider the applications of some \textit{arithmetic functions} by solving some selected questions. In order to achieve this, we shall use the "M\"{o}biuss Inversion theorem" as our tool.

Recall from Definition \eqref{def:size} that the size of $f$ is given by $|f| = q^{\mathrm{deg(f)} }.$ We define zeta-function as follows;
\begin{defn}
The Zeta function of $\mathbb{A}$ is defined as
\begin{align*}
\zeta_\mathbb{A}(s) = \sum_{\substack{{f\in\mathbb{A} }\\
							{\text{$f$ monic}}}}
\frac{1}{|f|^s}, \text{ for } s\in\mathbb{C}, ~~\mathbb{R} e(s)>1.
\end{align*} 
We know that the number of monic polynomials of degree $d$ in $\mathbb{A}$ is given by $q^{d }$. Thus, we have
\begin{align*}
\zeta_\mathbb{A}(s) = \sum_{\substack{{f\in\mathbb{A} }\\
							{\text{$f$ monic}}}}
q^{\text{-s~deg($f$)}}.
\end{align*}
Considering the sum of all monic irreducible polynomials, we have
\begin{align*}
\sum_{\text{deg($f$)}\leq d}|f|^{-s} = 1 + q^{1-s} + q^{2(1-s)} + \ldots +q^{d(1-s)},
\end{align*}
from which we get
\begin{align*}
\zeta_\mathbb{A}(s) = \sum_{d \geq 0} \frac{q^d}{q^{ds}}.
\end{align*}
This simplifies into
\begin{align}
\label{zt:1}
\zeta_\mathbb{A}(s) = \frac{1}{1-q^{1-s}}, \text{ for } \mathbb{R}e(s)>1.
\end{align}
Using the zeta function, Euler deduced the identity for the unique prime factorisation into integers as follows;
\begin{align}
\label{zt:t}
\zeta_\mathbb{A}(s) = \prod_{\substack{p\text{ prime}\\
                           p>0 }}\Big(1 - \frac{1}{p^s}\Big)^{-1}
\end{align}
which is valid only for $\mathbb{R}e(s)>1$. Using the fact that the irreducible polynomials are like the prime numbers, and applying Euler's formula of unique factorisation of $\zeta_\mathbb{A}(s)$ into irreducibles, we have
\begin{align}
\zeta_\mathbb{A}(s) =& \prod_{g\in\mathbb{A_+}}\Bigg(1 + \frac{1}{|g|^s} + \frac{1}{|g|^{2s}} + \ldots \Bigg),\\
=&\prod_{g\in\mathbb{A_+}}\Bigg(\sum_{i=0}^{\infty} \frac{1}{|g|^{is}}\Bigg),\\
=&\prod_{g\in\mathbb{A_+}}\Bigg(\sum_{i=0}^{\infty}\Big(\frac{1}{|g|^{s}}\Big)^i\Bigg)\label{zt:v}.
\end{align}
Let $x =\frac{1}{|g|^{s}}$. Then $\displaystyle{\sum_{i=1}^\infty}x^i$ is a geometric series. This series can either converge or diverge. It converges if $x<1$ and diverges if $x>1.$ We know that $|g|\geq 2$. Thus, $|g|^{s}\geq 2^s,$ but $\mathbb{R}e(s)>1$. Then
\begin{align*}
|g|^{s}\geq 2\Rightarrow \frac{1}{|g|^{s}}\leq \frac{1}{2}<1.
\end{align*}
Thus, the series converges. Hence, 
\begin{align}
\label{zt:a}
\sum_{i=0}^\infty x^i=\frac{1}{1-x}.
\end{align}
Substituting $|g|^{s}$ into Equation \eqref{zt:a}, we get,
\begin{align*}
\frac{1}{1-\frac{1}{|g|^s}}=\Big(1-\frac{1}{|g|^s}\Big)^{-1}.
\end{align*}
Then from Equation \eqref{zt:v}, we have a similar identity as that of Equation \eqref{zt:t} provided by Euler, 
\begin{align}
\label{zt:2}
\zeta_\mathbb{A}(s) = \prod_{g\in\mathbb{A_+}}\Big(1 - \frac{1}{|g|^s}\Big)^{-1},\text{ for $\mathbb{R}e(s)>1$. }
\end{align}
where $\mathbb{A_+}$ is set of all monic irreducible polynomials in $\mathbb{F}_q[T]$. We will use this notation in what follow.


As we will see shortly, Euler's function and Equation \eqref{zt:2} are very useful in this essay in as far as counting problems is concerned.
\end{defn}
 Let $x\in\mathbb{R}$ and let the function $\pi(x)$ define the number of positive primes at most $x.$ Then, the Prime Number theorem states that, in the set of integers, $\pi(x)$ is asymptotic to $\frac{x}{\log(x)}$. Our next aim is to find the explicit formula for the number of monic irreducible polynomials in the finite field $\mathbb{F}_q$. 
 
 Let $d\in \mathbb{Z}^+$ such that $q^{d} = |g|$. Also, let $\alpha_d$ be the number of monic irreducible polynomials of degree $d$. Then, using Equation \eqref{zt:2}, we have 
 \begin{align*}
 \zeta_\mathbb{A}(s) = \prod_{d=1}^{\infty}\Big( 1 - \frac{1}{|g^d|^s}\Big)^{-\alpha_d}.
 \end{align*}
 Substituting $|g|$ with $q^d$ in the above equation we get;
 \begin{align}
 \label{zt:3}
 \zeta_\mathbb{A}(s) = \prod^\infty_{d=1}\Big(1 - q^{-ds}\Big)^{-\alpha_d}.
 \end{align}
 From Equation \eqref{zt:1}, we have
 \begin{align}
 \zeta_\mathbb{A}(s) = \frac{1}{1 - q^{1-s}}.
 \end{align}
Let $v=q^{-s}.$ Then, Equation \eqref{zt:3} becomes 
\begin{align}
\label{zt:4}
\frac{1}{1-qv} = \prod^{\infty}_{d=1}\Big(1-v^d\Big)^{-\alpha_d}.
\end{align}
Differentiating Equation \eqref{zt:4} by means of logarithmic differentiation  with respect to $v$, we have;
\begin{align*}
-\log(1 - qv) = \sum_{d=1}^{\infty} -\alpha_d\log(1 - v^d)
\end{align*}
from which we get
\begin{align}
\label{e:end}
\frac{q}{1 - qv} = \sum_{d=1}^\infty\frac{\alpha_dd v^{d-1}}{1 - v^d}.
\end{align}
Multiplying Equation \eqref{e:end} by $v$, we get
\begin{align*}
\frac{vq}{1-qv} =& \sum^{\infty}_{d=1}\frac{d\alpha_dv^d}{1-v^d},
\end{align*}
\begin{align}
\label{zt:5}
qv\cdot\frac{1}{(1 - qv)} =& \sum_{d=1}^\infty d\alpha_d v^d(1 - v^d)^{-1}.
\end{align}
Expanding both side of Equation \eqref{zt:5} using geometric series, we get;
\begin{align*}
vq(1 + qv + q^2v^2 + q^3v^3 + \ldots) =& \sum_{d=1}^\infty d\alpha_dv^d(1 + v + v^2 + v^3\ldots),\\
qv + q^2v^2 + q^3v^3 + \ldots =& \sum_{d=1}^\infty d\alpha_d (v + v^2 + v^3 + \ldots),\\
q^nv^n = & \sum_{d|n}d\alpha_d v^n.
\end{align*}
Comparing the coefficients of $v^n,$ we get the following formula in Proposition \eqref{pro:mit} below;
\begin{pro}
\begin{align}
\label{pro:mit}
\sum_{d|n}d\alpha_d = q^n.
\end{align}
\end{pro}
%\begin{proof}
%See \citet{coster1990supercongruences}.
%\end{proof}

\begin{defn}
\label{de:mobius thm}(The M\"{o}bius $\mu$-function).
We define the M\"{o}bius $\mu$-function for $f\in\mathbb{A}$ $\mu(f)$ as;
\begin{align*}
\mu(f) = 
\begin{cases}
0, \text{ if } f \text{ is not square-free},\\
(-1)^{s}, \text{ if } f \text{ is the product of } s \text{ distinct irreducible polynomials}. 
\end{cases}
\end{align*}
\end{defn}

\begin{thm}[M\"{o}bius Inversion Theorem]
\label{th:mit}
If $F(n) = \displaystyle{\sum_{d|n}f(d)}, \text{ then, } f(n) =\displaystyle{\sum _{d|n}\mu(d)F(n/d)}$.
\begin{proof}
See \citet{ireland1972elements}.
\end{proof}
\begin{cor}
\label{co:con}
$\displaystyle{a_n = \displaystyle{\frac{1}{n}\sum_{d|n}\mu(d)q^{n/d}}}$
\end{cor}
where $a_n$ denotes the number of monic irreducible polynomials of degree $n$ in $\mathbb{F}_q[T].$
\end{thm}
\begin{proof}
 From Proposition \eqref{pro:mit}, we have $\displaystyle{\sum_{d|n}d\gamma_d = q^n}$ which corresponds to the M\"{o}bius inversion theorem with;
\begin{align*}
f(n) = q^n \text{ and } f(n) = n\alpha_n.
\end{align*} 
 Then, using the M\"{o}bius theorem, i.e. $f(n) = \displaystyle{\sum_{d|n}\mu(d)F(n/d)}$ and comparing the terms, we get
\begin{align}
\label{q:e.d}
na_n = \sum_{d|n}\mu(d)q^{n/d}.
\end{align}
Thus,
\begin{align*}
 \alpha_n = \frac{1}{n}\sum_{d|n}\mu(d)q^{n/d}.
\end{align*}
Hence, the corollary is proven.
\end{proof}
We have gathered adequate information needed to prove the main theorem of this section.
\begin{thm}[Prime number theorem for Polynomials]
\label{prim:nt}
Let $a_n$ denote the number of irreducible polynomials of degree $n$ in $\mathbb{F}_q[T]$. Then, 
\begin{align*}
a_n = \frac{q^n}{n} + O\bigg(\frac{q^{n/2}}{n} \bigg).
\end{align*}
\end{thm}
\begin{proof}
From Equation \eqref{q:e.d}, we observe that the highest power that $q$ can have is $q^n$ followed by $q^{n/2}$. The rest of the terms have powers of the form $\pm q^{m}$, where $m<n$ We observe that the number of terms is of the form $2^r,$ where $r$ is the number distinct prime divisors of $n$. Given the distinct primes dividing $n$, $\beta_1,\beta_2,\ldots,\beta_r$, we have $2^r \leq \beta_1\beta_2\cdots\beta_r\leq n$. Thus, we have the following estimate:
\begin{align}
\label{no:mip}
\Bigg|a_n - \frac{q^n}{n}\Bigg| \leq \frac{q^{n/2}}{n}
\end{align}
which simplifies into 
\begin{align}
\label{no:mip}
a_n = \frac{q^n}{n} + O\bigg(\frac{q^{n/2}}{n} \bigg).
\end{align}
here $a_n$ is the number of monic irreducible polynomials in $\mathbb{A}$. Let $q^n = x$. Substituting in Equation \eqref{no:mip}, we get
\begin{align}
\label{o:nom}
a_n = \frac{x}{log_q(x)} + O\Bigg(\frac{\sqrt{x}}{log_q(x)}\Bigg).
\end{align}
\end{proof}

\textbf{Remark:} From Equation \eqref{o:nom}, we see that we have an error of $\displaystyle{\frac{\sqrt{x}}{log(x)}}$. This error means that irreducible polynomials behave like \textit{random distribution.} For a polynomial of degree $n$ to be an irreducible, there is a probability of $\displaystyle{\frac{1}{d}}$. Therefore, we conclude that there is a marked similarity to the known prime number theorem.

Our next objective is to look at yet another counting problem. It is the concept of \textit{square-free.} By definition, an integer $\epsilon$ is said to be square-free if no square number greater than $1$ divides $\epsilon.$ In the ring $\mathbb{F}_q[T],$ the analogue of square-free numbers are known as \textit{square-free monics.} Therefore, our goal is to find out if the probability of finding square-free monics is also equal to $\frac{6}{\pi^2}$ just like in the ring $\mathbb{Z}.$

\begin{pro}
\label{zt:p}
The number of square-free monic polynomials in $\mathbb{A}$ of degree $d$ is given by
\begin{align*}
\beta_d = 
\begin{cases}
q^d(1 - q^{-1}) \text{ if } d>1,\\
q ~~~~~~~~~~~~~~ \text{ if } d = 1,\\
1 ~~~~~~~~~~~~~~\text{ if } d = 0.
\end{cases}
\end{align*}
\end{pro}
\begin{proof}
We consider the following Euler product for integers for $\mathbb{R}e(s)>1$;
\begin{align*}
\prod_P\Bigg(1+\frac{1}{P^s}\Bigg)=&\Bigg(1+\frac{1}{2^s}\Bigg)\Bigg(1+\frac{1}{3^s}\Bigg)\Bigg(1+\frac{1}{5^s}\Bigg)\Bigg(1+\frac{1}{7^s}\Bigg)\cdots,\\
=&\Bigg(1+\frac{1}{3^s}+\frac{1}{2^s}+\frac{1}{6^s}\Bigg)\Bigg(1+\frac{1}{7^s}+\frac{1}{5^s}+\frac{1}{35^s}\Bigg)\cdots,\\
=&1+\frac{1}{7^s}+\frac{1}{5^s}+\frac{1}{35^s}+\frac{1}{3^s}+\frac{1}{21^s}+\frac{1}{15^s}+\frac{1}{105^s}+\frac{1}{2^s}+\frac{1}{14^s}\cdots,\\
=&1+\frac{1}{2^s}+\frac{1}{3^s}+\frac{1}{5^s}+\frac{1}{6^s}+\frac{1}{7^s}+\frac{1}{8^s}+\frac{1}{10^s}+\cdots
\end{align*}
Using the similar reasoning for Euler product, where $g$ is irreducible polynomial, we have;
\begin{align*}
\prod_g\Big(1 + \frac{1}{|g|^{s}}\Big) =& \Bigg(1 + \frac{1}{|f|^s}\Bigg)\Bigg(1+\frac{1}{|f|^{2s}}\Bigg)\Bigg(1+\frac{1}{|f|^{3s}}\Bigg)\cdots,\\
=&\Big(1+\frac{1}{|f|^{2s}}+\frac{1}{|f|^s} + \frac{1}{|f|^{6s}}\Big)\Big(1+\frac{1}{|f|^{3s}} \Big)\cdots,\\
=&1+\frac{1}{|f|^{2s}}+\frac{1}{|f|^s}+\frac{1}{|f|^{3s}}+\frac{1}{|f|^{5s}}+\frac{1}{|f|^{4s}}+\frac{1}{|f|^{6s}}+\cdots,
\end{align*}
When we take the sum for all $\mathbb{R}e(s)>1$, we have,
\begin{align*}
\prod_g\Big(1 + \frac{1}{|g|^{s}}\Big)=&1+\frac{1}{|f|^s}+\frac{1}{|f|^{2s}}+\frac{1}{|f|^{3s}}+\frac{1}{|f|^{4s}}+\frac{1}{|f|^{5s}}+\frac{1}{|f|^{6s}}+\cdots.
\end{align*}
This simplifies into the identity below;
\begin{align}
\label{zt:6}
\prod_g\Big(1 + \frac{1}{|g|^{s}}\Big) = \sum_{\mathbb{A}_+}\frac{|\mu(f)|}{|f|^{s}},
\end{align}
where $g$ and $f$ range over all monic irreducible polynomials. Then,
\begin{align*}
%
\prod_g\Bigg(\frac{1 - \Big(\frac{1}{|g|^s}\Big)^2}{1 - \frac{1}{|g|^s}}\Bigg) =& \sum_{f\in\mathbb{A}_+}|\mu(f)||f|^{-s},\\
\frac{\displaystyle{\prod_g}\Big(1 - \frac{1}{|g|^{s}}\Big)^{-1}}{\displaystyle{\prod_g}\Big(1 - \frac{1}{|g|^{2s}}\Big)^{-1}} =& \sum_f|\mu(f)|q^{-s}\deg(f).\\
\end{align*}
The LHS simplifies into;
\begin{align*}
\frac{\zeta_{\mathbb{A}}(s)}{\zeta_{\mathbb{A}(2s)}} =& \sum_{d=0}^{\infty}\sum_{f \text{ of } \deg d} |\mu(f)|q^{-s}\deg(f),\\
&=\sum_{d=0}^{\infty}q^{-s}\cdot\underbrace{\sum_{f \text{ of }\deg d}|\mu(f)|}_{\beta_d},\\
\frac{\zeta_{\mathbb{A}}(s)}{\zeta_{\mathbb{A}(2s)}}&=\sum_{d=0}^{\infty}\beta_d q^{-sd}.
\end{align*}
Therefore, from \eqref{zt:1}, we have;
\begin{align*}
\frac{1 - q^{2-s}}{1 - q^{2-s}} = \sum_{d=0}^\infty\beta_d\cdot (1 - q^{1-s})^{-1}.\\
\end{align*}
Let $v = q^{s}$, then substituting in the equation above, we have;
\begin{align}
\label{zz:1}
\frac{1 - qv^2}{1 - qv} = \sum_{d=0}^\infty\beta_d(1 - qv)^{-1}.
\end{align}
Expanding both sides of Equation \eqref{zz:1} and  using the geometric series, we get;
\begin{align*}
(1 - qv)(1 + qv + q^2v^2 + \cdots) =& \sum_{d=0}^\infty\beta (1 + qv + qv^2 + \cdots),\\
1 + qv + q^2v^2 + \cdots - qv^2 - q^2v^3 - q^3v^4 - \cdots =& \beta_d v^d,\\
1 + qv + (q^2 - q)v^2 + (q^3 - q^2)v^3 + \cdot =& \beta_dv^d,
\end{align*}
which simplifies into;
\begin{align*}
q^d(1 - q^{-1})v^d =& \beta_dv^d
\end{align*}
after comparing the coefficients of $v^d$. This implies that 
\begin{align}
\label{Eq:sqf}
\beta_d = q^d(1 - q^{-1}).
\end{align}
Then it follows that,
\begin{align*}
\beta_d = 
\begin{cases}
q^d(1 - q^{-1}) \text{ if } d>1,\\
q ~~~~~~~~~~~~~~ \text{ if } d = 1,\\
1 ~~~~~~~~~~~~~~\text{ if } d = 0.
\end{cases}
\end{align*}
\end{proof}
\textbf{Remark}: Equation \eqref{Eq:sqf} shows that the probability that a monic polynomial is square-free is
\begin{align*}
\frac{\beta_d}{q^d} = 1 - q^{-1} = \frac{1}{\zeta_\mathbb{A}(2s)}.
\end{align*}
But in classical Number theory, it is known that the probability that an integer $n$ is square-free is
\begin{align}\label{zt:b}
B_d = \frac{1}{\zeta(2)} = \frac{6x}{\pi^2} .
\end{align}
Thus, we conclude that a strong similarity exists between the \textit{square-free numbers} in $\mathbb{Z}$ and the number of \textit{square-free monic polynomials} in $\mathbb{F}_q[T]$.

Our next subject is to introduce some familiar functions and discuss their properties.

Let $\sigma_0(f)$ be the number of monic divisors of $f$ and $\sigma_1(f)$ be the sum of monic divisors of $f$. These functions, as we shall explore shortly, have a \textit{multiplicity property}.
\begin{defn}
Let $f$ and $g$ be relatively prime. Then, a complex valued function $\sigma$ defined on $\mathbb{A}{\backslash\{0\}}$ is said to be \textit{multiplicative} if 
\begin{align*}
\sigma(fg) = \sigma(f)\sigma(g).
\end{align*}
Suppose that
\begin{align}
\label{mt:1}
f = \alpha g_1^{r_1}g_2^{r_2}\cdots g_s^{r_s}
\end{align}
is a given prime decomposition, then $\sigma$ is multiplicative if 
\begin{align*}
\sigma(f) = \sigma(g_1^{r_1})\sigma(g_2^{r_2})\cdots\sigma(g_s^{r_s}).
\end{align*}
\end{defn}
Therefore, we can say that multiplicative functions are computed entirely by the indices on the primes.

The multiplicative property is useful because one can then derive various formulas. Below are some of the formulas that can be formulated.
\begin{pro}
Suppose that we have a prime decomposition $f$ as in \eqref{mt:1} then, 
\begin{itemize}
\item[(i)]$\phi(f) = |f|\displaystyle\prod_{g|f}(1 - |g|^{-1})$, where $g$ is a monic irreducible polynomial.
\item[(ii)] $\sigma_0(f) = (r_1 + 1)(r_2 + 1)\cdots(r_s + 1)$.
\item[(iii)]$\displaystyle{\sigma_1(f) = \frac{|g_1|^{r_1+1} - 1}{|g_1| - 1}\cdot\frac{|g_2|^{r_2+1} - 1}{|g_2| - 1}\cdots\frac{|g_s|^{r+1} - 1}{|g_s| - 1}}$.
\end{itemize}
\begin{proof}
\hfill
\begin{itemize}
\item[(i)] We already found the formula for $\phi(f)$ in Proposition \eqref{mtb:2}.
\item[(ii)]If $g$ is a monic irreducible polynomial, then the divisors of $g^{r}$ are $1, g, g^2,\ldots,g^r.$ Then it follows that
\begin{align}
\label{mt:3}
\sigma_0(g^r) = r + 1.
\end{align}
\item[(iii)] It is clear from (ii) that
\begin{align*}
\sigma_1(g^{r}) = 1 + |g| + |g|^2 + \ldots + |g|^r = \frac{(|g|^{r+1} - 1)}{|g| - 1}.
\end{align*}
\end{itemize}
Hence, the formula for $\sigma_1(f)$ follows.

\end{proof}
\end{pro}
\section{Application of Riemann Zeta Function and Arithmetic Functions}
In this section, we will apply the Riemann zeta function and arithmetic functions to solve some selected questions in \citet{rosen2002number} on page 19 Exercise 2.

\section*{Application 1}
\label{q:5}
\begin{align*}
\text{Show that } \sum_{m\in\mathbb{A}}|m|^{-1} \text{  diverges, where the sum is over all monic polynomials } m\in\mathbb{A}. 
\end{align*}
\textbf{\textit{Solution:}}

Let  $ I = \displaystyle{\sum_{m\in\mathbb{A}}}|m|^{-1}$, be the sum over all monic polynomials. $\mathbb{A} = \mathbb{F}_q[T]$ and $\mathbb{F} $ have $q$ elements. We know that any field has at least $2$ elements, namely; the neutral elements for addition and multiplication. Consider,
\begin{align*}
I = \sum_{\text{deg($m$)}=0}|m|^{-1} + \sum_{\text{deg($m$)}=1}|m|^{-1} + \sum_{\text{deg($m$)}=2}|m|^{-1} + \ldots .
\end{align*}
For $ m\in\mathbb{A}$, where $m$ is monic of degree $k$, we have;
\begin{align*}
m_k = T^k + \alpha_1T^{k-1} + \alpha_2T^{k-2} + \ldots + \alpha_kT^0.
\end{align*}
For fixed $k$, we look for the number of polynomials $m_k$, i.e. we need the number of \textit{$k$-tuples} $(\alpha_1,\ldots,\alpha_k)$ with $\alpha_i\in\mathbb{F}.$ But $\mathbb{F}$ has $q$ elements. So, we have 
\begin{align*}
\underbrace{ q\times q\times\cdots\times q}_{k~\text{times}} = q^k.
\end{align*}
 On the other hand, $|m| = q^{\text{deg}(m)}$ by definition. Thus, 
\begin{align*}
I =& \sum_{\text{deg($m$)}=0}q^{-\text{deg}(m)} + \sum_{\text{deg($m$)}=1}q^{-\text{deg}(m)} + \sum_{\text{deg($m$)}=2}q^{-\text{deg}(m)} + \ldots\\
 = &\sum_{\text{deg($m$)}=0} 1 + \sum_{\text{deg($m$)}=1}q^{-1} + \sum_{\text{deg($m$)}=2}q^{-2} + \ldots\\
 = & q^0\times 1 + q^1\times q^{-1} + q^2\times q^{-2} + \ldots\\
 = & 1 + \sum_{k=1}^{\infty} q^kq^{-k} =
1 + \sum_{k=1}^\infty 1, \text{ which diverges.}
\end{align*}
%Hence, $\displaystyle{\sum_{m\in\mathbb{A}}|m|^{-1}}$ diverges. 

\section*{Application 2}
\textit{Use the fact that every monic polynomial can be written uniquely in the form $m = m_0m_1,$ where $m_0$ and $m_1$ are monic and $m_0$ is square-free to show $\sum|m|^{-1}$ diverges where the sum is over all square-free monics $m_0.$}

\textbf{\textit{Solution:}}
Let
\begin{align*}
\sum_{m\in\mathbb{A}_+}|m|^{-1} =& \sum_{\substack{{m_0\cdot m_1\in\mathbb{A}_+ }\\
							{m_0~\text{square-free}}}}|m_0\cdot m_1^2| = \sum q^{-\text{deg}(m_0\cdot m_1^2)} \\
=& \sum_{\substack{{m_0,m_1\in\mathbb{A}_+ }\\
							{m_0~\text{square-free}}}} q^{-\text{deg($m_0$)}}\cdot q^{-\text{deg($m_1^2$)}} \\
=& \sum_{m_0,m_1\in\mathbb{A}_+} q^{-\text{deg($m_0$)}}\cdot q^{-2\text{deg}(m_1)} \\
=& \sum_{\substack{{m_0,m_1\in\mathbb{A}_+ }\\
							{m_0~\text{square-free}}}} |m_0|^{-1}\cdot (q^2)^{-\text{deg}(m_1)} \\
=&\sum_{\substack{m_0\in\mathbb{A}_+\\ m_0~ \text{square-free}}}\Bigg(\sum_{m_1\in\mathbb{A}_+}(q^2)^{-\text{deg}(m_1)}\Bigg)|m_0|^{-1}.
\end{align*}
Consider
\begin{align*}
J =& \sum_{m_1\in\mathbb{A}_+}(q^2)^{-\text{deg($m_1$)}}\\
 =& \sum_{\text{deg($m_1$)}=0}(q^2)^0 + \sum_{\text{deg($m_1$)}=1}(q^2)^{-1} + \sum_{\text{deg($m_1$)}=2}(q^2)^{-2} + \ldots\\
 =& q^0 + q^{-2} + q^{-4} + \ldots\\
 =& 1 + \sum_{i=1}q^{-2i}\\
 =&  \frac{1}{1 - q^{-2}}, \text{ a geometric series}.
\end{align*}
 \text{ This geometric series converges to some constant, say $v$. Thus, we have, } $J=v.$
 
Therefore,
\begin{align*}
\sum_{m\in\mathbb{A}_+}|m|^{-1} = \sum_{m_0} J\cdot |m_0|^{-1} = v\sum_{m_0~\text{square-free}}|m_0|^{-1}.
\end{align*}
 We know from Question 5 that $\displaystyle{\sum_{m}|m|^{-1}}$ diverges. Thus, $\displaystyle{\sum_{m_0~\text{square-free}}}|m_0|^{-1}$ diverges. Hence, shown.
\section*{Application 3}
\textit{Application 2 to show that}
\begin{align*}
\prod_{\substack{P~\text{ irreducible}\\ \deg P\leq d}}\Big(1 + |P|^{-1}\Big) \rightarrow \infty \text{ as } d \rightarrow \infty. 
\end{align*}
\textit{\textbf{Solution:}}

Using the idea of prime factorisation used in proving Proposition \eqref{zt:p}, we have;
\begin{align*}
\prod_{\substack{P~\text{ irreducible}\\ \deg P\leq d}}\Big(1 + |P|^{-1}\Big) =&\Big(1+\frac{1}{2}\Big)\Big(1+\frac{1}{3}\Big)\Big(1+\frac{1}{5}\Big)\Big(1+\frac{1}{7}\Big)+\cdots,\\
=&1+\frac{1}{2}+\frac{1}{3}+\frac{1}{5}+\frac{1}{6}+\frac{1}{7}+\cdots,\\
=&\sum_{\substack{m~\text{ square-free}}}\frac{1}{|m|}.
\end{align*}
Therefore, we have,
\begin{align*}
\prod_{\substack{P~ \text{irreducible}\\\deg P\leq d}}\Big(1 + |P|^{-1}\Big) =& \sum_{\substack{m~\text{ square-free}}}|m|^{-1}.
\end{align*}
From Application 2: Question 6, 

$\displaystyle{\sum_{\substack{m~\text{ square-free}}}|m|^{-1}}$ diverges. Thus, $\displaystyle{\prod_{\substack{P~\text{ irreducible}\\ \deg P\leq d}}\Big(1 + |P|^{-1}\Big)}$ diverges too.
\section*{Application 4}
Use the obvious inequality $1 + x \leq e^x$ and \textbf{Question 7 in Application 3} to show that $\displaystyle{\sum|P|^{-1}}$ diverges, where the sum is over all monic irreducible polynomials $P\in\mathbb{A}$.

\textit{\textbf{Solution:}}

Consider the following;
\begin{align*}
\prod_{\substack{P~ \text{irreducible}\\ \deg P\leq d}}\Big(1 + |P|^{-1}\Big) \leq& \prod_{\substack{P~ \text{irreducible}\\ \deg P\leq d}}\exp\bigg({\frac{1}{|P|}}\bigg),\\
=&\exp\Bigg({\sum_{\substack{P~ \text{irreducible}\\ \deg P\leq d}}\frac{1}{|P|}}\Bigg).\\
\end{align*} 
Therefore, since $\displaystyle{\prod_{\substack{P~ \text{irreducible}\\ \deg P\leq d}}\Big(1 + |P|^{-1}\Big)}$ diverges, it follows that $\displaystyle{\exp\Bigg({\sum_{\substack{P~ \text{irreducible}\\ \deg P\leq d}}\frac{1}{|P|}}\Bigg)}$ diverges too. Hence, $\displaystyle{\sum|P|^{-1}}$ diverges.

\section*{Application 5}

\textit{Use Equation \eqref{no:mip} i.e.\big($a_n = \frac{q^n}{n} + O(q^{n/3}$\big) in Theorem \eqref{prim:nt} to give another proof that $\sum|P|^{-1}$ diverges. }

\textbf{\textit{Solution:}}
\begin{align*}
\sum_{\substack{{P~ \text{irreducible}}\\P~ \text{monic}}}|P|^{-1} =& \sum_{\text{deg($P$)}=0}|P|^{-1} + \sum_{\text{deg($P$)}=1}|P|^{-1} + \sum_{\text{deg($P$)}=2}|P|^{-1} + \ldots,\\
 =& \sum_{\text{deg($P$)}=0}q^{-\text{deg($P$)}} + \sum_{\text{deg($P$)}=1}q^{-\text{deg($P$)}} + \sum_{\text{deg($P$)}=2}q^{-\text{deg($P$)}} + \ldots,\\
 =& \sum_{\text{deg($P$)}=0}q^{0} + \sum_{\text{deg($P$)}=1}q^{-1} + \sum_{\text{deg($P$)}=2}q^{-2} + \ldots.
\end{align*}
Then, from the same theorem, we have;
\begin{align*}
\sum_P|P|^{-1} = a_0\times q^{0} + a_1\times q^{-1} + a_2\times q^{-2} + \ldots,
\end{align*}
This simplifies into; 
\begin{align*}
\sum_p|P|^{-1} = \sum_{n=0}\frac{a_n}{q^n}.
\end{align*}
Replacing $a_n$ by its estimate, we have;


\begin{align*}
\sum_p|P|^{-1} =& \sum_{n=1}^{\infty}\Bigg(\frac{q^n}{n} + O\bigg(\frac{q^{n/2}}{n}\bigg)\Bigg)q^{-n},\\
\sum_p|P|^{-1} =& \sum_{n=1}^{\infty}\Bigg(\frac{1}{n} + O\Big(\frac{q^{n/2}}{n}\Big)q^{-n}\Bigg),\\
\sum_p|P|^{-1} =& \sum_{n=1}^{\infty}\frac{1}{n} + O\sum\Big(\frac{q^{-n/2}}{n}\Big)=\sum_{n=0}^\infty\frac{1}{n} + O(1),\\
\end{align*}
since $\displaystyle{\sum\frac{1}{nq^{n/2}}}$ converges. Note also that $\displaystyle{\sum_{n=1}^\infty\frac{1}{n}}$ diverges.
\begin{align*}
\end{align*}
Thus, $\displaystyle{\sum_P|P|^{-1} }$ diverges.

















