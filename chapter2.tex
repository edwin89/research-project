\chapter{Similarities between the ring $\mathbb{Z}$ and the ring $\mathbb{F}_q[T]$ of polynomials over a finite field} \label{ch:2}
\section{Unique Factorization}
In this section, our main aim is to state and prove the \textit{fundamental theorem of arithmetic  for polynomials}. But before we do so, let us first gather the necessary tools that we need. 

\begin{defn}
Let $f_1,f_2\in\mathbb{A}$. For $f_2 \neq 0,$ we say that $f_1$ is divisible by $f_2$ if there is a $g \in \mathbb{A}$ such that $f_1 =f_2g.$
\end{defn}
\begin{lem}
Let $\alpha$ and $\beta$ be integers with $\beta \geq 1$. Then, $\exists j,k \in \mathbb{A}$ such that $\alpha = \beta j + k$ with $0\leq k < \beta$.
\end{lem}

We will not provide a proof for this lemma (see \citet{ireland1972elements}, p 4.), but rather, we shall prove Proposition \eqref{po:poly ed} which is the same statement for  polynomials. 

\begin{pro}
\label{po:poly ed}
Let $f$ and $g$ be polynomials in $\mathbb{F}_q[T]$. If $g$ is non-zero, then there exist polynomials $j,k\in\mathbb{F}_q[T]$ such that 
\begin{align*}
f = gj + k \text{   and that~~either  } k  = 0 \text{  or   } \deg(k) < \deg(g).
\end{align*}
\end{pro}
\begin{proof}
Let $c=sgn(f)$ be the leading coefficient of the polynomial $f$, and $d$ = sgn$(g)$ be the leading coefficient of the polynomial $g$. First, suppose that $g|f$, then we just set $j = \frac{f}{g}$ and $r = 0$. Next, suppose that $g\notdivides f$, then assume that $k = f - hg$ is the polynomial with the smallest degree out of all the polynomials of the form $f -hg$ for some $h\in\mathbb{F}_q[T]$. We need to consider two parts. The first one is when $\deg(f)<\deg(g)$ and the second one is when $\deg(f)\geq\deg(g)$. For the first part, when $\deg(f)<\deg(g)$, set $j=0$ and $k = f$, then we are done. For the second one, if $\deg(f)\geq\deg(g)$, then we observe that the degree of 
\begin{align*}
k - cd^{-1}T^{\deg(f)-\deg(g)}g = f - (j + cd^{-1}T^{\deg(f)-\deg(g)})g
\end{align*}
is smaller than that of $k$ and it is of the form stated above. Thus, we have a contradiction to our assumption that $k$ has the smallest degree out of all polynomials.



% We will prove by induction on the degree of $f$. We need to consider two parts. The first part is when $\deg(f) < \deg(g)$ and the second one is when $\deg(f)\geq\deg(g)$.
%
%For the first part, set $h = 0$ and $k = f$, then we are done. For the second, if $\deg(f)>\deg(g)$, we observe that the degree of 
%\begin{align*}
%f_1\coloneqq f - \frac{c}{d}T^{\deg(f)-\deg(g)}g
%\end{align*}
% is smaller than that of $f$. Thus, by induction, there exist polynomials $h_1,k_1 \in \mathbb{F}_q[T]$ such that 
%\begin{align*}
% f_1 =gh_1 + k_1
%\end{align*} 
%with $k_1 = 0$ or $\deg(f_1)<\deg(g)$. Therefore, we set 
%\begin{align*}
%h = cd^{-1}T^{\deg(f)-\deg(g)} + h_1
%\end{align*}
%and $k=k_1$. This completes the proof.
\end{proof}

\begin{defn}
\label{definition:Ideal}
If $\mathbf{R}$ is a commutative ring, then we call a non-empty subset $I$ of $\mathbf{R}$ an \textit{ideal} of $ \mathbf{R}$ if the following properties are met;
\begin{itemize}
\item[(i)] $\forall \alpha,\beta \in I,\alpha \pm \beta \in I, $
\item[(ii)] $ \forall\alpha\in I$, $\forall r\in \mathbf{R},r\alpha \in I,$
\item[(iii)] $0\in I$.
\end{itemize}

\end{defn}

%\begin{defn}
%\label{def:gene}
%Let $s_1,s_2,...,s_n \in\mathbb{A}$. Also, let $G\subseteq \mathbb{A}$. We say that the set $(s_1,s_2,...,s_n)$ generates $G$ if for all $t_1,t_2,...,t_n\in \mathbb{A}$, we have
%\begin{align}
%s_1t_1 +s_2t_2+...+ s_nt_n\in G.
%\end{align}
%
%\end{defn}

\begin{defn}
\label{de:ideal}
Suppose that $s_1,s_2,...,s_n\in \mathbb{A}$. Then we define $\langle s_1,s_2,...,s_n\rangle$ to be the set of all integers or polynomials of the form 
\begin{align}
s_1t_1 +s_2t_2 +...+s_nt_n,
\end{align}
for $t_1,t_2,...,t_n \in\mathbb{A}$.

Therefore, we can see that
$s_1,s_2,\ldots,s_n$ generates the ideal $I\subset \mathbb{A}$ if: $\forall g\in I, \exists t_1,t_2,\ldots,t_n \in \mathbb{A}$ such that 
\begin{align*}
g = s_1t_1 +s_2t_2 +...+s_nt_n.
\end{align*}
\end{defn}

%\textbf{Remark:} By the definition of an ideal, we can notice that the set $G$ is an ideal because we can take any two elements in $G,$ the sum and the difference will also be in $G$. In short, the set $G$ satisfies the properties of Definition \eqref{de:ideal}.
\begin{defn}
\label{df:gcd}
Let $f,g\in \mathbb{A}$. Then we say that $h\in \mathbb{A}$ is the \textit{greatest common divisor} \textit{(gcd)} of $f$ and $g$ if $h$ divides both $f$ and $g$ and that any other divisor of elements $f$ and $g$ divides $h$ as well. 

In this essay, we denote $(f,g)$ to be the set of elements $f$ and $g$ which are common divisors of $f$ and $g$. 
\end{defn}
\textbf{Note:} We can determine the greatest common divisor of two given integers up to sign while for the polynomials, we can determine up to multiplication by a constant.

\begin{lem}
Let $f,g\in \mathbb{A}$. Then there exists an element $h_1 \in \mathbb{A}$ such that $(f,g) = \langle h_1\rangle$. 
\end{lem}

\begin{proof}
We will offer a proof on the elements in $\mathbb{F}_q[T]$.
Let $h_1 \in (f,g)$ be a non-zero polynomial with the smallest degree. It is clear that $\langle h_1\rangle \subseteq (f,g)$. What remains is to show the reverse inclusion. Suppose that $h_2 \in (f,g)$, where $h_1 \notdivides h_2$. Then there exist polynomials $j,k\in\mathbb{A}$ such that $$h_2 = jh_1 + k,$$ with $\deg(k) < \deg(h_1)$. But $h_1, h_2 \in (f,g)$. Thus, we have 
\begin{align*}
k = h_2 - jh_1 \subseteq (f,g).
\end{align*}
 Since $\deg(k)<\deg(h_1),$ we have a contradiction from the fact that $h_1$ is a polynomial of least degree. This means that, $h_1|h_2$ and that $h_2\in \langle h_1\rangle$. Thus, $(f_1,g)=\langle h_1\rangle$.
\end{proof}

\begin{defn}
Let $f\in \mathbb{F}_q[T]$. If $f$ is non-zero and we cannot write it as a product of two polynomials of smaller degrees, then we say that $f$ is \textit{irreducible}. On the other hand, an element $p\in\mathbb{Z},~~ p>1$ is said to be a \textit{prime} if it has only two positive factors, $1$ and $p$.
\end{defn}

\begin{lem}
Suppose that $f_1,f_2 \in\mathbb{A}$. If $(f_1,f_2) = \langle g\rangle$, then $g$ is a \textit{greatest common divisor} of $f_1$ and $f_2.$ 
\end{lem}
\begin{proof}
We know that $f_1,f_2 \in \langle g\rangle.$ Then we have $g|f_1$ and $g|f_2$. Let $h \neq 0 \in \mathbb{A}$ such that $h|f_1$ and $h|f_2.$ Then, it is clear that $h$ divides every integer or polynomial of the form 
\begin{align*}
f_1j + f_2k \text{ with } j,k\in \mathbb{A}.
\end{align*}
 In particular, $h|g.$
\end{proof}

\begin{defn}
\label{def:relat prm}
If the only divisors of two polynomials $f_1$ and $f_2$ are constants, then we say that $f_1$ and $f_2$ are relatively prime. Particularly, $(f_1,f_2) = \mathbb{A^*}$.
\end{defn}

\begin{pro}
\label{pro:eclid}
Let $g$ be a prime. If 
\begin{align*}
g|f_1f_2, \text{  then  } g|f_1 \text{  or  } g|f_2.
\end{align*}
\end{pro}

\begin{proof}
Since $g$ is prime, then the divisors of  $g$ are 1 and $g$ itself. Therefore, we have
\begin{align*}
 (f_1,g) = \langle g\rangle \text{ or }(f_1,g) = \langle 1\rangle.
\end{align*}
 We have two parts to consider. Firstly, if $g|f_1$, then we have nothing to prove. Secondly, $g$ and $f_1$ are relatively prime. Then by Definition \eqref{def:relat prm}, we have,
\begin{align*}
 (f_1,g) = \langle 1\rangle.
\end{align*} 
This implies that there exist polynomials $j$ and $k$ such that $f_1j + gk = 1$. Hence,
\begin{align}
\label{de:be eclid}
 f_1jf_2 + gkf_2 = f_2.
\end{align}
 The results follow since $g$ divides the left-hand-side of the equation and it must divide the right hand side too.
\end{proof}

\begin{defn}
Given that $g$ is prime, then we define an integer $\alpha$ such that $g^\alpha|f$ but $g^{\alpha+1}\notdivides f$ to be the order of the polynomial $f$, denoted as \textit{$ord_gf$.}
\end{defn}
\begin{cor}
Let $f_1,f_2\in\mathbb{A}$ and $g\in\mathbb{A}$ be a prime. Then 
\begin{align*}
ord_gf_1f_2 = ord_gf_1 + ord_gf_2.
\end{align*}
\end{cor}
\begin{proof}
let $t = ord_gf_1$ and $r = ord_gf_2.$ Then, we have $f_1 = g^{t}\alpha$ and $f_2 = g^{r}\beta,$ where $g \not\divides \alpha$ and $g \notdivides \beta$. Then, $f_1f_2 = g^{t+r}\alpha\beta$. Thus,
\begin{align*}
ord_gf_1f_2 = t + r = ord_gf_1 + od_gf_2.
\end{align*}
\end{proof}

%\begin{defn}
%\label{de:order}
%$
%ord_r =
%\begin{cases}
%1 \text{  if  } r = 1
%\\
%0 \text{  if  } r \neq 1
%\end{cases}$
%\end{defn}


We have gathered now enough information to give a proof of the main theorem of this section. 
\begin{thm}{(Fundamental Theorem of Arithmetic)}.
\label{th:conc}
Every non-zero integer or non-constant polynomial can be expressed as a product of primes or monic irreducible polynomials as follows:
\begin{align}
\label{th:FTA}
f = \gamma\prod_gg^{h(g)},
\end{align}
where the product is over all primes or monic irreducible polynomials. The exponent $h(g)$ and the constant $\gamma$ are  uniquely determined. Actually, $h(g) = ord_g  f.$ 
\end{thm}

\begin{proof}
We will prove this theorem from the polynomial point of view. Our proof is by induction on the degree of polynomials. We can easily observe that polynomials of degree one are irreducible. Suppose that the results are true for all polynomials whose degrees are less than $\deg(f)$. If $f$ is a monic irreducible polynomial, then we have nothing to prove. But if not, then let $f = g\gamma$, where $1 \leq \deg(g),$ $\deg(\gamma) < \deg(f)$. By induction, both polynomials $g$ and $\gamma$ are a product of irreducibles. Therefore, $f = g\gamma$ is also a product of irreducibles.

Let us now prove how uniquely the exponents are determined. We get the following result after applying the $ord_r$ function to both sides of Equation \eqref{th:FTA};
\begin{align}
ord_r f = ord_r\gamma + \sum_gh(g) ord_rg.
\end{align}
By definition, $ord_rc = 0$ (since $c$ is a constant) and $ord_rg = 0$ if $g\neq r$ and 1 if $g = r$. Thus, we have $ord_rf = h(r)$. This concludes the proof.
\end{proof}

\section{Prime Factorisation in Principal Ideal Domain (PID)}
What is the relationship between the ring of integers, $\mathbb{Z}$ and the polynomial ring, $\mathbb{F}_q[T]$ in a principal ideal domain? Why is the study of principal ideal domains important in this essay? This section is dedicated to answer these questions and bring out some similarities between these two rings.  

\begin{defn}[Euclidean domain]
\label{def:PID}
R is said to be a \textit{Euclidean domain} if there is a function $f$  that maps R$\backslash\{0\}$  to $\mathbb{N}$ with the following properties;
\end{defn}
\begin{itemize}
\item[]
\begin{enumerate}[(i)]
\item $f(\alpha) \leq f(\alpha\beta), \forall \alpha,\beta \in  $ R.
\item $\forall\alpha,\beta\in  $ R, and $\beta \neq 0,\exists $ $j$ and $k$ such that $\alpha = \beta j + k$ and that either $k = 0$ or $f(k) < f(\beta)$.
\end{enumerate}
\end{itemize}

As we shall see soon, the idea of Euclidean domain is very important as it reduces the task of proving that many rings are principal ideal domains. 

\begin{defn}
\label{def:size}
 Suppose $f \in \mathbb{A}$. If $f$ is non-zero, then $|f| = q^{\deg(f)}$. If $f = 0$, then $|f| = 0$.

\textbf{Remark:}
Note that $|f|$ determines the size of $f$. We can generalise this as follows. If $n$ is any integer, then $|n|$ is the number of elements of the quotient ring $\mathbb{Z}/n\mathbb{Z}$ (see the definition of $\mathbb{Z}/n\mathbb{Z}$ in section \ref{sec:2.4}). In the same vein, $|f|$ is the number of elements in $\mathbb{A}/f\mathbb{A}$. Therefore, if $f_1$ and $f_2$ are polynomials, then $|f_1f_2| = |f_1||f_2|$ and if $|f_1| \ne\ |f_2|$, then $|f_1 + f_2| \leq \text{max}(|f_1|,|f_2|)$.
\end{defn}

\begin{pro}
\label{r:ideal}
Suppose that R is a Euclidean domain and $I\subseteq$ R is an ideal. Then, there exists an  element $\alpha \in$ R such that $I =$ R$\alpha = \langle\alpha\rangle$.
\end{pro}
\begin{proof}
Let us consider a set of positive integers; 
\begin{align}
\label{set:int}
\{f(\beta):\beta\in I, \beta \neq 0 \}. 
\end{align}
Each set of the form in \eqref{set:int} above contains a least element. Let $\alpha \in I, \alpha \neq 0$ be such an element such that $f(\alpha) \leq f(\beta)$ for all $\beta \in I, \beta \neq 0.$ We want to show that $I = \langle\alpha\rangle.$ To do so, we will show that $\langle\alpha\rangle \subseteq I$ and that $I\subseteq\langle\alpha\rangle$. It is straightforward that $I\subseteq\langle\alpha\rangle$. Let $\beta \in I$, then by Definition \eqref{de:be eclid}, there exist elements $j,k \in $ R such that $\beta =\alpha j +k$, with $k = 0$ or $f(k) < f(\alpha).$ But because $k = \alpha - \beta j \in I$, then it is impossible to have $f(k)<f(\alpha)$. Hence, $k = 0$ and $\beta = \alpha j \in \langle\alpha\rangle$. Thus, $I\subseteq \langle\alpha\rangle$. Since we have shown that $I\subseteq\langle\alpha\rangle$ and $\langle\alpha\rangle \subseteq I$, this proves that $I = \langle\alpha\rangle.$
\end{proof}
\textbf{Remark:} If $\alpha_1,...,\alpha _n \in $ R, then $\langle \alpha_1,...,\alpha_n \rangle = $ R$\alpha_1 +\cdots +$R$\alpha_n$ is called an ideal. If $I = \langle \alpha\rangle$, for some $\alpha \in I,$ then $I$ is called a \textit{principal ideal. }
\begin{defn}
If all ideals in R are principal, then R is called a \textit{principal ideal domain.}
\end{defn}
\textbf{Remark:} Proposition \eqref{r:ideal} shows that all Euclidean domains are \textit{PIDs} but the converse of this statement is not true.

Note that $\mathbb{Z}$ and $\mathbb{F}_q[T]$ are Euclidean domains. Every Euclidean domain is a PID and every PID has a unique factorisation.

Let us now turn our attention and introduce some new terms in \textit{PIDs.} We say that an element $p \in$ R is \textit{prime} if for $
p \neq 0,~  p|\alpha\beta \Rightarrow p|\alpha \text{ or } p|\beta.$
 An element $u$ is said to be a \textit{unit } if it divides $1$.  If $\alpha = \beta u,$ where $u$ is a unit, then we say that $\alpha$ and $\beta$ are associated. An element $\gamma \in $ R is \textit{irreducible} if $\alpha|\gamma \Rightarrow \alpha$ is an associate of $\gamma$ or a unit.

We can observe that the difference between irreducible and prime is a new concept. These concepts coincide in the ring $\mathbb{Z}$ and the polynomial ring $\mathbb{F}_q[T]$.

Let us translate some of the concepts into ideals' language. So, $\alpha|\beta$ if and only if $\langle \beta\rangle \subseteq \langle\alpha\rangle$. An element $u \in $ R is a unit if and only if $\langle u\rangle = $ R. We say that $\alpha$ and $\beta$ are \textit{associated} if and only if $\langle\alpha\rangle = \langle\beta\rangle$. $p$ is said to be a \textit{prime} if and only if $\alpha\beta \in \langle p\rangle$ means that either $\alpha \in \langle\alpha\rangle$ or $\beta \in \langle\beta\rangle$.

\begin{cor}
\label{co:copid}
Let R be a principal ideal domain and let $\alpha,\beta\in $ R be relatively prime. Then, $(\alpha,\beta) = $ R.
\end{cor}
Check \citet{ireland1972elements} p.10 for the proof.
\begin{cor}
Let R be a principal ideal domain and let $\gamma \in $ R be irreducible. Then $\gamma$ is prime.
\end{cor}
\begin{proof}
Let $\gamma|\alpha\beta$ and let $\gamma \notdivides \alpha$. Then, because $\gamma\notdivides\alpha$, it follows that the only common divisors are the units. By Corollary \eqref{co:copid} $(\alpha,\beta) = $ R, we can write $(\alpha\gamma,\beta\gamma) = \langle\beta\rangle.$ But because $\alpha\beta \in \langle\gamma\rangle$ and $\beta\gamma\in \langle\gamma\rangle,$ then $\langle\beta\rangle \subseteq \langle\gamma\rangle$. Hence, $\gamma|\beta$. It can easily be observed that a prime is irreducible.
\end{proof}

Henceforth, we can notice that in \textit{PIDs,} prime is the same as irreducible. Therefore, in the section that follows, we will use these terms interchangeably, unless otherwise indicated.

\begin{lem}
\label{lem:chinideas}
Consider the following ascending chain of ideals in a \textit{PID};
\begin{align*}
\alpha_1\subseteq \alpha_2\subseteq \alpha_3...\subseteq.
\end{align*}
Then, there exists an $r\in\mathbb{Z}$ such that 
\begin{align*}
\langle \alpha_r\rangle = \langle \alpha_{r+1}\rangle,
\end{align*}
 for $r = 0,1,2,....$ Particularly, this chain terminates after finitely many steps.
\end{lem}

\begin{proof}
Suppose that $I = \displaystyle{\bigcup^\infty_{i=1}\langle \alpha_i\rangle}$. We can easily verify that $I$ satisfies the properties of an ideal outlined on Definition \eqref{definition:Ideal}. Hence, $\exists \alpha \in$ R with $I = \langle \alpha\rangle$. But
\begin{align*}
\alpha\in \bigcup^\infty_{i=1}\langle \alpha_i\rangle\Rightarrow   \alpha \in \langle \alpha_r\rangle \text{ for  some}~~ r.
\end{align*}
This entails that
$I = \langle \alpha\rangle \subseteq \langle\alpha_r\rangle.$
  Then, it follows that
\begin{align*}
 I = \langle \alpha_r\rangle = \langle \alpha_{r+1}\rangle =\ldots.
\end{align*}  
  
\end{proof}

\begin{pro}
Every element of R which is both a non-zero and non-unit is a \textit{product of irreducibles.}
\end{pro}

\begin{proof}
Given that $\alpha \in$ R, $\alpha \neq 0$ is a non-unit. We will show that; if $\alpha \in \mathbb{R}, \alpha \neq 0,$ then there is an irreducible polynomial that divides $\alpha$
and $\alpha$ is a product of irreducibles.

Let us consider the first part. If $\alpha$ is irreducible, then we have nothing to prove. If not, then let $\alpha = \alpha_1\beta_1$, where $\alpha_1$ and $\beta_1$ are not units. Similarly, if $\alpha_1$ is irreducible, then we have nothing to prove. If not, let $\alpha_1 = \alpha_2\beta_2,$  where $\alpha_2$ and $\beta_2$ are not units. Again, if $\alpha_2$ is irreducible, then we have nothing to prove. If not, let $\alpha_2 = \alpha_3\beta_3$, where $\alpha_3$ and $\beta_3$ are not units. We can carry on this process in finitely many steps. Thus, we have
\begin{align*}
\langle\alpha\rangle \subset \langle \alpha_1\rangle\subset \langle\alpha_2\rangle\subset \langle\alpha_3\rangle\subset\ldots .
\end{align*}
By Lemma \eqref{lem:chinideas}, this chain terminates after finitely many steps. Hence, for some $r,$ $\alpha_r$ is irreducible.

Next, let us consider the second part of the proof. If $\alpha$ is irreducible, then we have nothing to prove. If not, let $\beta_1$ be irreducible such that $\beta_1|\alpha.$ Then, we have $\alpha = \beta_1 u.$ As usual, if $u$ is a unit, then we have nothing to prove. If not, let $\beta_2$ be irreducible such that $\beta_2|u_1.$ Then, we have $\alpha =\beta_1\beta_2 u.$ Similarly, if $u_2$ is a unit, then we have nothing to prove. Then we can continue with the same process as before. Thus, we have
\begin{align*}
\langle\alpha \rangle \subset \langle u_1\rangle\subset \langle u_2\rangle \subset \langle u_3\rangle\subset\ldots .
\end{align*}
By Lemma \eqref{lem:chinideas}, this chain terminates in finitely many steps. Thus, $\alpha$ is a product of irreducibles. %\citet{ireland1972elements}.
\end{proof}


Consider the following set $\mathcal{P}\subset$ R of prime elements such that;

for every prime $\alpha \in$ R, there exists a prime $\beta \in \mathcal{P}$ such that $\alpha$ and $\beta$ are associates.

 In order to have a set with such properties, we should choose one prime from each set of associate primes. We can have a wider choice in ring the $\mathbb{Z}$ and in the ring $\mathbb{F}_q[T]$. In ring $\mathbb{Z}$, we can form set $\mathcal{P}$ by choosing positive primes while in polynomial ring $\mathbb{F}_q[T]$, monic irreducible polynomials would serve the purpose. Generally, there is no specific method of forming the set $\mathcal{P}$ because it occasionally becomes complicated. The theorem below gives us the way on how we can make such a choice.
 \begin{thm}
 Suppose that R is a \textit{PID} and that the set $\mathcal{P}$ contains primes with the properties stated above. Then, for $f\in$ R, $f \neq 0,$ we can write
 \begin{align}
 f = u\prod_\beta\beta^{h(\beta)},
 \end{align}
 where the product is over all $\beta\in \mathcal{P}$ and $u$ is a unit and the order $h(\beta)$ is uniquely determined.
 \end{thm}

 For the proof, see \citet{ireland1972elements} p.12.
 
\section{Congruences}
\label{sec:2.4}
In the previous section, we discussed the concept of \textit{prime decomposition} in a \textit{PID}. We have not discussed how many primes or monic irreducible polynomials there are in the ring $\mathbb{A}$. The first part of this section addresses that concern. The rest of the section discusses congruences.
\begin{thm}
In the ring $\mathbb{A}$, there are infinitely many  primes.
\end{thm}
\begin{proof}
Let $f_1 = g_1,g_2,\ldots,g_s$ be a finite list of primes in $\mathbb{A}$. We will show that at least one more prime not in our list exists. Consider the product of primes $f_1 = g_1g_2\cdots g_s$. Let $f_2 = f_1 +1$. Then, we have to consider two parts of the proof; either $f$ is prime or not.

Firstly, if $f_2$ is prime, then at least one more prime exists other than those in our list above.
Secondly, $f_2$ is not prime, then $f_2$ is divisible by some prime factor $g$. Suppose that this factor $g$ were part of the factors of $f_1$, then $f_1$ would be divisible by $g$ (as $f_1$ is the product of the $g_i$'s) but $g|(f_1 + 1) = f_2$.

If $g|f_1$ and $g|f_2$, then 
\begin{align*}
g|(f_1 - f_2)\Rightarrow g|(f_1 - f_1 +1) = 1.
\end{align*}
This is a contradiction because no prime divides 1. Thus, $g$ is not part of our list. This implies that there is at least one more prime which is not one of those in our list. This proves the claim that $\mathbb{A}$ has infinitely many primes. 

\end{proof}

Recall that in a Euclidean domain, two elements $\alpha$ and $\beta$ are associated if $\alpha = \beta u$, where $u$ is a unit. At the moment, we know that in $\mathbb{Z}$ and $\mathbb{F}_q[T],$ there are infinitely many primes which are not associated.
\begin{defn}
\label{de:congr}
Let $f_1,f_2,g\in\mathbb{A}$, with $g \neq 0$. Then, $f_1$ is said to be \textit{congruent to $f_2$ modulo $g,$} denoted as $f_1 \equiv f_2\pmod g$, if $g|(f_2-f_1)$.
\end{defn}
\begin{pro}
\label{def:congr}
The following properties hold for both the ring $\mathbb{Z}$ and the polynomial ring $\mathbb{F}_q[T]$;
\begin{itemize}
\item[(i)] $f_1 \equiv f_1 \pmod g$.
\item[(ii)] If $f_1 \equiv f_2 \pmod g$ then $f_2 \equiv f_1 \pmod g$.
\item[(iii)] If $f_1 \equiv f_2 \pmod g$ and $f_2 \equiv f_3,$ then $f_1 \equiv f_3 \pmod g$.
\end{itemize}
\end{pro}
\begin{proof}
By using Definition \eqref{de:congr}, we see that;
\begin{itemize}
\item[(i)] $f_1 - f_2 = 0$ and $g|0$.
\item[(ii)] If $g|(f_2 - f_1),$ then, $g|(f_1 - f_2)$.
 \item[(iii)] If $g|(f_2 - f_1)$ and $g|(f_3 - f_2),$ then, $g|(f_3 -f_1) = (f_3 - f_2) + (f_2 - f_1)$. Therefore, $g|(f_3 - f_1).$
\end{itemize}
\end{proof}

Proposition \eqref{def:congr} entails that if $f_2\in\mathbb{A}$, then we denote $\bar{f_2}$ to be the set of integers or polynomials congruent to $f_2$ modulo $g,$i.e. $\bar{f_2} = \{f_1 \in \mathbb{A}: f_1 \equiv f_2 \pmod g\}$. Particularly, $\bar{f_2}$ is a set of integers or polynomials of the form $f_2 + gh.$
\begin{pro}
Given $f_1,f_2,g \in\mathbb{A}$, we have the following conditions:
\begin{itemize}
\item[(a)] $\bar{f_1} = \bar{f_2}$ if and only if $f_1 \equiv f_2\pmod g$.
\item[(b)] $\bar{f_1} \neq \bar{f_2}$ if and only if $f_1 \bigcap f_2$ is empty.
\end{itemize}
\end{pro}
\begin{proof}
let us consider all cases as claimed above in the proposition;
\begin{itemize}
\item[(a)] $(\Rightarrow)$: Firstly, suppose that $\bar{f_2} = \bar{f_1}$,
 then 
$ f_1 \in \bar{f_1} = \bar{f_2}.$
 So, 
\begin{align*}
f_1 \equiv f_2 \pmod g.
\end{align*} 
 

$(\Leftarrow)$: Secondly, suppose that 
\begin{align*}
f_1 \equiv f_2\pmod g,
\end{align*}
 then $f_1 \in \bar{f_2}$. Let $f_3 \in\mathbb{A},$ such that
\begin{align*}
f_3 \equiv f_1\pmod g,
\end{align*} 
   then
\begin{align*}
 f_3 \equiv f_2 \Rightarrow \bar{f_1} \subseteq \bar{f_2}.
\end{align*}   
    Because $f_1 \equiv f_2\pmod g \Rightarrow f_2 \equiv f_1 \pmod g$ and also $\bar{f_2} \subseteq \bar{f_1}.$ Thus, $\bar{f_1} = \bar{f_2}.$
\item[(b)] It is obvious that if $\bar{f_1}\bigcap \bar{f_2} = \emptyset$, then $\bar{f_1} \neq \bar{f_2}$. We want show that
\begin{align*}
\bar{f_1}\bigcap \bar{f_2} \neq \emptyset \Rightarrow \bar{f_1} = \bar{f_2}.
\end{align*}
  Let $f_3 \in \bar{f_1}\bigcap \bar{f_2}.$ Then, $f_3 \equiv f_1 \pmod g$ and also $f_3 \equiv f_2 \pmod g$. It follows that 
\begin{align*}
 f_1 \equiv f_2 \pmod g
\end{align*}  
  and thus, by part (a) above, $\bar{f_1}= \bar{f_2}.$
\end{itemize}
\end{proof}
\begin{defn}
We denote the set of congruence classes modulo $g$ as $\mathbb{A}/g\mathbb{A}$. Suppose that $(\bar{f_1},\bar{f_2},\ldots, \bar{f_n})$ contains all congruence classes modulo $g,$ then we call $\{f_1,f_2,\ldots,f_n\}$ a \textit{complete residue  set modulo $g$}.
\end{defn}

\section{Structure of $\mathbb{A}/g\mathbb{A}$ and its units}
\label{quotient :ring}
This section is dedicated to describe the structure of the ring $\mathbb{A}/g\mathbb{A}$. To accomplish this, we will use the \textit{so called Chinese Remainder Theorem.}
\begin{defn}
\label{de:ring}
A \textit{field } is defined to be a ring in which each non-zero element is a unit.
\end{defn}

\begin{pro}
\label{pro:ring}
$\mathbb{A}/g\mathbb{A}$ is a field if and only if $g$ is prime. $\bar{f}\in\mathbb{A}/g\mathbb{A}$ is a unit if and only if $f$ and $g$ are relatively prime and it follows that $\mathbb{A}/g\mathbb{A}$ has $\phi(g)$ units, where $\phi(g)$ denotes the number of units in quotient $\mathbb{A}/g\mathbb{A}$.
\end{pro}
For the proof, see \citet{ireland1972elements} p. 33.

Definition \eqref{de:ring} and Proposition \eqref{pro:ring} can be interpreted in ring theoretical terms. It turns out that given the ring $\mathbb{A}/g\mathbb{A}$, then $\alpha x \equiv \beta\pmod g$ is equivalent to the relation $\bar{\alpha}x = \bar{\beta}.$
\begin{pro}
Suppose that $g\in \mathbb{A}$ is prime. Then the group $(\mathbb{A}/g\mathbb{A})^*$ is \textit{cyclic} with $|g|-1$ elements.
\end{pro}
\begin{proof}
Since $\mathbb{A}$ is a $PID$, then it is clear that $\mathbb{A}/g\mathbb{A}$ is a field because $g\mathbb{A}$ is a maximal ideal. Thus, we conclude that $(\mathbb{A}/g\mathbb{A})^*$ is a cyclic group with $|g|-1$ number of elements. 
\end{proof}

Now, what are the units of the structure $\mathbb{A}/g\mathbb{A}?$ If $\alpha$ and $g$ are relatively prime, then the congruence $\alpha x \equiv 1 \pmod g$ is \textit{solvable.} Hence, we can easily see that if $gcd(\alpha ,g) = 1$, then an element
\begin{align*}
0\neq\bar{\alpha}\in\mathbb{A}/g\mathbb{A}
\end{align*}
  is a unit. 

\begin{thm}[The Chinese Remainder Theorem]
 \label{thm:crm}
 Let $\beta = \beta_1\beta_2\cdots\beta_s$ and $gcd(\beta_i,\beta_j) = 1,$ for $i\neq j$. Suppose that $g_1,g_2,\ldots,g_s\in \mathbb{Z}$. Then there exists $\alpha\in\mathbb{Z}$ satisfying the following;
 \begin{align*}
 \alpha \equiv \beta_1 (mod~ g_1),  \alpha \equiv \beta_2 (mod~ g_2)\cdots \alpha \equiv \beta_s (mod ~g_s).
 \end{align*}
 \end{thm}

We will not prove this theorem, but rather, we shall interpret it in ring language. Given that 
$\mathbb{S} = (H_1, H_2,\ldots,H_s)$ are rings, then the set of \textit{n-tuples} $(h_1,h_2,\ldots,h_s)$ with $h_i\in H_i$ is the \textit{direct sum} of $H_i$. We define addition as;
\begin{align*}
(h_1,h_2,\ldots,h_s) + (h_1^{'}, h_2^{'},\ldots,h_s^{'}) = (h_1+h^{'}_1, h_2+h^{'}_2,\ldots,h_s+h^{'}_s).
\end{align*}
Similarly, multiplication is defined as;
\begin{align*}
(h_1,h_2,\ldots,h_s)\cdot (h_1^{'}, h_2^{'},\ldots,h_s^{'}) = (h_1\cdot h_1^{'}, h_2\cdot h_2^{'},\ldots , h_s\cdot h_s^{'}).
\end{align*}
\begin{align*}
(0,0,\ldots, 0) \text{  and  } (1,1,\ldots, 1)
\end{align*}
are the \textit{zero} and \textit{identity} elements respectively. An element $\mathcal{U}\in\mathbb{S}$
is a unit if and only if there exists an element $\mathcal{V}\in\mathbb{S}$ such that $\mathcal{U}\mathcal{V} = 1$. Suppose that $\mathcal{U} = (u_1,u_2,\ldots,u_s)$ and $\mathcal{V} = (v_1,v_2,\ldots, v_s)$, then $\mathcal{U}\mathcal{V}=1$ implies that for each $1\leq s\leq n$, we have  $\mathcal{U}_s\mathcal{V}_s = 1.$ Therefore, $\mathcal{U}_s$ is a unit for every $s.$ 

On the other hand, suppose that $\mathcal{U}_s$ is a unit for every $s$. Then, denoting $H^*$ to be the \textit{group} of units, $\mathcal{U}(H_1)\times\mathcal{U}(H_2)\times \cdots\times\mathcal{U}(H_s)$ is the set of units of \textit{n-tuples} $(u_1,u_2,\ldots,u_s)$ for some $\mathcal{U}_s\in H_s.$ Thus, we have a group under component-wise multiplication.

\begin{pro}
Suppose that $h_1, h_2,\ldots,h_s\in\mathbb{A}$. We denote $\psi_i$ to be the natural \textit{homomorphism} from $\mathbb{A}/h\mathbb{A}$ to $\mathbb{A}/h_s\mathbb{A}$. Then, the map
\begin{align*}
\psi:\mathbb{A}/h\mathbb{A}\longrightarrow \mathbb{A}/h_1\mathbb{A}\oplus\mathbb{A}/h_2\mathbb{A}\oplus\cdots \oplus\mathbb{A}/h_s\mathbb{A}
\end{align*}
defined by,
\begin{align*}
\psi(\beta) = (\psi_1(\beta), \psi_2(\beta),\ldots,\psi_s(\beta)), \forall \beta\in\mathbb{A}
\end{align*}
is a ring homomorphism.
\end{pro}
\begin{proof}
This is a standard result and holds in any $PID.$
\end{proof}
\begin{cor}
\label{mtt}
The map $\psi$ confined to the units of $\mathbb{A}, \mathbb{A}^{*},$ induces a group isomorphism
\begin{align}
\label{co:mtb2}
(\mathbb{A}/h\mathbb{A})^*\simeq (\mathbb{A}/h_1\mathbb{A})^{*}\times (\mathbb{A}/h_2\mathbb{A})^{*}\times \cdots\times(\mathbb{A}/h_s\mathbb{A})^{*}
\end{align}
where the $h_i$s are relatively prime.
\end{cor}
\begin{proof}
See \citet{ireland1972elements} p. 35.
\end{proof}

Now, given that $f = \alpha g^{r_1}_1g^{r_2}_2\cdots g^{r_s}_s$ has given the prime decomposition. Using the above idea, we can write;
\begin{align*}
(\mathbb{A}/f\mathbb{A})^*\simeq (\mathbb{A}g^{r_1}_1\mathbb{A})^*\times (\mathbb{A}g^{r_2}_2\mathbb{A})^*\times\cdots \times (\mathbb{A}g^{r_s}_s\mathbb{A})^*.
\end{align*}
We can easily determine the structure of the group $(\mathbb{A}/g^r\mathbb{A})^*$ where $g$ is an irreducible polynomial and $r\in\mathbb{Z}^+$. When $\mathbb{A}=\mathbb{F}_q[T]$ and $r = 1,$ we have that $(\mathbb{A}/g\mathbb{A})^*$ equals to $\mathbb{Z}$ as $(\mathbb{Z}/g\mathbb{Z})$, where $g$ is prime.

%\begin{pro}
%\label{pro:units}
%Suppose that $g$ is an irreducible polynomial. Then, the group $(\mathbb{A}/g\mathbb{A})^*$ is \textit{cyclic} with $|g|-1$ elements.
%\end{pro}
%\begin{proof}
%We know that $\mathbb{A}$ is a $PID$. Then, it is clear that $\mathbb{A}/g\mathbb{A}$ is a field because $q\mathbb{A}$ is a maximal ideal. Thus, by Lagrange Theorem, $(\mathbb{A}/g\mathbb{A}^*)$ is cyclic group with order $\phi(g) = |g|-1$. %See the definition for $\phi(g)$ in \citet{gallian2010contemporary}.
%\end{proof}

\begin{pro}
\label{mtb:1}
Suppose that $g \in \mathbb{A}$ is an irreducible polynomial and that $r$ is a positive integer. Then $(\mathbb{A}/g^r\mathbb{A})^{(1)}$ is the kernel of the natural map
\begin{align*}
(\mathbb{A}/g^r\mathbb{A})^* \longrightarrow (\mathbb{A}/g\mathbb{A})^*.
\end{align*}
Hence, $(\mathbb{A}/g\mathbb{A})^{(1)}$ has order $|g|^{r-1}$.
\end{pro}
\begin{proof} 
Since $(\mathbb{A}/g^r\mathbb{A})$ is a local ring, then it has a single maximal ideal $g\mathbb{A}/g^r\mathbb{A}$ which has $|g|^{r-1}$ elements.
 Since $(\mathbb{A}/g^r\mathbb{A})^* \longrightarrow (\mathbb{A}/g\mathbb{A})^*$
is surjective and that $(\mathbb{A}/g\mathbb{A})^*$ has $|g| - 1$ elements then, the claim about the order of $(\mathbb{A}/g^r\mathbb{A})^{(1)}$ follows immediately.
\end{proof}
 \begin{pro}
 \label{mtb:2}
 \begin{align*}
 \phi(f) = |f|\prod_{g|f}\Bigg(1 - \frac{1}{|g|}\Bigg).
 \end{align*}
 \end{pro}
 \begin{proof}
 Let 
 $f = \alpha g_1^{r_1}q_2^{r_2}\cdots g_s^{r_s}$
 be the prime decomposition of $f$. Then, we have 
 \begin{align*}
 \phi(f) 
=& \Bigg(\alpha|g_1^{r_1}g_2^{r_2}\cdots g_s|^{r_s}\Bigg)\Bigg(1-\frac{1}{|g_1|}\Bigg)\Bigg(1-\frac{1}{|g_2|}\Bigg)\cdots\Bigg(1-\frac{1}{|g_s|}\Bigg),\\
=&\alpha\Bigg(|g_1|^{r_1}|g_2|^{r_2}\cdots|g_s|^{r_s}\Bigg)\Bigg(1-\frac{1}{|g_1|}\Bigg)\Bigg(1-\frac{1}{|g_2|}\Bigg)\cdots\Bigg(1-\frac{1}{|g_s|}\Bigg),\\
=&\alpha\Bigg(|g_1|^{r_1}-\frac{|g_1|^{r_1}}{|g_1|}\Bigg)\Bigg(|g_2|^{r_2}-\frac{|g_2|^{r_2}}{|g_2|}\Bigg)\cdots\Bigg(|g_s|^{r_s}-\frac{|g_s|^{r_s}}{|g_s|}\Bigg).
 \end{align*}
 As the constant $\alpha$ does not affect the size of the polynomial, the above expression simplifies into,
 \begin{align*}
 \phi(f) = \prod_{i=1}^\infty\Bigg(|g|^{r_s}-|g_s|^{r_s-1}\Bigg).
 \end{align*}
 
 \end{proof}
 
 
 \textbf{Remark:}  We can see that there is a strong analogue between the formula in proposition \eqref{mtb:2} and the classical formula for $\phi(n).$
 
 
 
We have discussed the structure of rings and their units. Our next target is to discuss some famous theorems that describe the number of elements in these groups. The next section will do so.
\section{Euler's Theorem, Fermat's Little Theorem and Wilson's Theorem}
\begin{thm}[Euler's Theorem]
Let $\alpha,g\in \mathbb{A}$. If the $gcd(\alpha,g) = 1,$ then
\begin{align*}
\alpha^{\phi(g)} \equiv 1\pmod g.
\end{align*}
\end{thm}
\begin{proof}
The set $(\mathbb{A}/g\mathbb{A})^*$ is the group of units and by Lagrange's Theorem, $(\mathbb{A}/g\mathbb{A})^*$ is a cyclic group with order $\phi(g) = |g|-1$. Thus, if $gcd(\alpha,g) = 1,$ then $\bar{\alpha}$ is also a unit. Then, we have
\begin{align*}
\bar{\alpha}^{\phi(g)} = \bar{1} \text{  or } \alpha^{\phi(g)}\equiv 1 \pmod g.
\end{align*}
\end{proof}
Note that $[\alpha]$ denote the elements of $(\mathbb{A}/g\mathbb{A})^*$.

\begin{cor}[Fermat's Little Theorem]
For $\alpha \in \mathbb{A}$, we define $|\alpha|$ to be the number of elements in $\mathbb{A}/\alpha\mathbb{A}$. Let $g$ be prime and $\alpha\in\mathbb{A}$ with $g\notdivides \alpha.$ Then, 
\begin{align*}
\alpha^{|g|-1} \equiv 1\pmod g.
\end{align*}
\end{cor}
\begin{proof}
Since $g \not\divides \alpha$, then $[\alpha] \in (\mathbb{A}/g\mathbb{A})^*$ which has an order $|g|-1$. Therefore, by Lagrange's Theorem, 
\begin{align*}
[\alpha]^{|g|-1} = [1], \text{  that is, } \alpha^{|g|-1}\equiv 1 \pmod g.
\end{align*}
\end{proof}


 \begin{cor}[Wilson's Theorem]
 Let $g$  be an integer greater than $1$. Then $g$ is prime if and only if $(g-1)!\equiv -1\pmod g.$
 \end{cor}
 \begin{proof}
We will prove the polynomial version of this theorem.
%For the case when $q$ is even, i.e $q=2$, it is straightforward. We will prove the case when $q$ is greater than 2. Let $q$ be prime. We are required to prove that $(q-1)!\equiv-1\pmod q$. If $\beta \in\{1,2,3,\ldots,q-1\}$, then, the we have
%
%\begin{align*}
%\beta x \equiv 1\pmod q
%\end{align*}
%has a particular solution $\beta^{'}\in \{1,2,3,\ldots,q-1\}$. Let $\beta = \beta^{'}$ then, $\beta^2\equiv 1\pmod q$. Thus, 
%\begin{align*}
%q|\beta^2-1=(\beta - 1)(\beta + 1) \Rightarrow q|(\beta - 1)~~\text{  or  }  q|(\beta + 1), \text{     hence,  } \beta\in \{1,q-1\}
%\end{align*}
%
%Thus, the elements 
%\begin{align}
%\label{q:wil}
%2.3.4\ldots (q-2)\equiv 1\pmod q
%\end{align}
% can each be paired by its inverse. Hence, when we multiply Equation \eqref{q:wil} by $q-1!$, we get 
% \begin{align*}
% (q-1)!\equiv -1\pmod q
% \end{align*}
%  as stated.
% 
% Next, let $(q-1)!\equiv -1\pmod q$. We will prove that $q$ is prime. Suppose that $q$ is not a prime so that $q\geq 4$. Let $\gamma$ be a prime number that divides $q$. Then, it is clear that $\gamma < q$ and that $\gamma|(q-1)!$. Further, by assumption,
% \begin{align*}
% \gamma|q|((q-1)!+1).
% \end{align*}
% Hence, we have a contradiction as a prime number can not divide a number say $r$ as well as $r+1$.
 \end{proof}
 

 \begin{cor}[Wilson's Theorem in {$\mathbb{F}_q[T]$}]
 Suppose that $g\in \mathbb{F}_q[T]$ is an irreducible polynomial of degree $\gamma$ and that $\mathcal{X}$ is a variable. Then 
 \begin{align*}
 \mathcal{X} ^{|g|-1} -1 \equiv \prod_{0\leq \deg(f_1)<\gamma}(\mathcal{X} - f_1) \pmod g.
 \end{align*}
 
 \begin{proof}
We know that $f_1\in\mathbb{F}_q[T]$ such that $\deg(f_1)<\gamma$ represents the elements of $\mathbb{A}/g\mathbb{A}$. Thus, we have
\begin{align*}
\mathcal{X}^{|g|-1} - \bar{1} = \prod_{0\leq \deg(f_1)<d}(\mathcal{X} - \bar{f_1})
\end{align*}
where the bars denote respective the cosets modulo $g$. This is true because both the LHS and the RHS of the above equation are composed of monic irreducible polynomials in $\mathcal{X}$ whose roots are the elements of $\mathbb{A}/g\mathbb{A}$. By the fact that the number of roots is $|g|-1$ on both sides, the difference between the LHS and the RHS is 0. This is the same as the congruence claimed in the corollary.
 \end{proof}
 \end{cor}

